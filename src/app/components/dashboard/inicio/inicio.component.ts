import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Editor, Toolbar } from 'ngx-editor';
import { comentariosI, foroI } from 'src/app/interfaces/all-interface.interface';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {



  constructor(private router : Router) { 

  }

  ngOnInit(): void {
  }



  goToForos(){
    this.router.navigate(['/foros'])
  }
}
