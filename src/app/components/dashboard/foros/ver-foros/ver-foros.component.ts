import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Editor, Toolbar } from 'ngx-editor';
import { comentariosI, foroI } from 'src/app/interfaces/all-interface.interface';
import { ComentariosService } from 'src/app/services/comentarios.service';
import { ForosService } from 'src/app/services/foros.service';
import { UsuariosService } from 'src/app/services/usuarios.service';

@Component({
  selector: 'app-ver-foros',
  templateUrl: './ver-foros.component.html',
  styleUrls: ['./ver-foros.component.css']
})
export class VerForosComponent implements OnInit {

  number : number = 0
  html: string = '';
  listaTemas !: foroI[];
  foro!: foroI;
  listaComentarios !: comentariosI[];
  comentario = '';
  mostrarComentarios: boolean = true;
  comentarioRes!: string;
  IDforo!: number;

  editor: Editor;
  toolbar: Toolbar = [
    ['bold', 'italic'],
    ['underline', 'strike'],
    ['code', 'blockquote'],
    ['ordered_list', 'bullet_list'],
    [{ heading: ['h1', 'h2', 'h3', 'h4', 'h5', 'h6'] }],
    ['link', 'image'],
    ['text_color', 'background_color'],
    ['align_left', 'align_center', 'align_right', 'align_justify'],
  ]

  propiedades: any = {
    error: false
  }
  constructor(private _foroSVC : ForosService,
    private _usuariosSVC : UsuariosService,
    private activateRoute: ActivatedRoute,
    private _comentarioSVC : ComentariosService,
    private router : Router) { 

      this.editor = new Editor(); //se instancia el editor

      this.activateRoute.params.subscribe(params => {
        const id : number = params['id'];
        let number: number = Number(id);
        this.IDforo = number
        console.log(number);
        console.log('entra a ver un Foro');
  
        let encontrado = this._foroSVC.obtenerUnForo(number);
        console.log(encontrado);
        
        if(encontrado === undefined){
          this.html = 'FORO NO ENCONTRADO'
        } else{
      this.foro = encontrado;
      this.html = encontrado.foro;
    }
      });
  
      //COMENTARIO
      if(localStorage.getItem("Comentarios")?.length === 0){
        console.log('no comments');
      } else {
        this.mostrarComentarios = this.verComentarios()
      }
  }

  ngOnInit(): void {
  }

  guardar(){
    // console.log(this.comentario);
    let fecha = new Date();
    // console.log(fecha);

    let hora = fecha.toLocaleTimeString()
    let dia = fecha.toLocaleDateString()
    let nombre = this._usuariosSVC.mostrarUser();
    
    if(nombre === null){
      console.log('no se pudo');
      
    } else {

      let comentid = Math.floor(Math.random() * 1000)
      console.log(comentid);

    const comentario : comentariosI = {
      comentid: comentid,
      foroid: this.foro.foroid,
      usuario: nombre,
      fecha: { dia, hora },
      comentario: this.comentario,
    }
    console.log(comentario);
    this._comentarioSVC.agregarNew(comentario)
    this._usuariosSVC.snackbar(3)
    }

  }


  verComentarios(): boolean{

    // let ojo = this._comentarioSVC.obtenerLocalStorage()
    // console.log(ojo);
    
    if(localStorage.getItem("Comentarios") === null){
      console.log('no hay comentarios');
      return false
    } else {
      console.log('si hay comentarios');
      this.listaComentarios = JSON.parse(localStorage.getItem("Comentarios") || '')
      let finder = this.listaComentarios.filter(data => {
        return data.foroid === this.IDforo;
      });

      this.listaComentarios = finder
      console.log(finder);

      return true
    }
  }

  //LIKES
  sumar(){
    if(this.propiedades.error === false){
      this.number = this.number - 1
    } else {
      this.number = this.number + 1
    }
  }

}
