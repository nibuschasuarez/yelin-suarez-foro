import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VerForosComponent } from './ver-foros.component';

describe('VerForosComponent', () => {
  let component: VerForosComponent;
  let fixture: ComponentFixture<VerForosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VerForosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VerForosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
