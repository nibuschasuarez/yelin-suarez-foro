import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { foroI, UsuarioRegistradoI } from 'src/app/interfaces/all-interface.interface';
import { ForosService } from 'src/app/services/foros.service';
import { UsuariosService } from 'src/app/services/usuarios.service';

@Component({
  selector: 'app-foros',
  templateUrl: './foros.component.html',
  styleUrls: ['./foros.component.css']
})
export class ForosComponent implements OnInit {
    
  listaTemas : foroI[] = [];
  html: string = '';
  cantidad_mensajes = 0
  foro!: foroI;
  listaUsuarios!: UsuarioRegistradoI[];

  constructor(private _foroSVC : ForosService, 
              private router : Router,
              private _usuariosSVC : UsuariosService) {

  // if (localStorage.getItem("Foros")=== null){
  //   console.log('no hay temas');
    
  // } else {
  //   console.log('deberia haber temas');
    
  //   this.verForo()
  // }


}

  ngOnInit(): void {

    this.traerUsuarios()
  }

  verForo(){
    if(this._foroSVC.obtenerLocalStorage() === null){
      this.listaTemas = []
    } else {
      this.listaTemas = this._foroSVC.obtenerLocalStorage()
      console.log('Mostro????');
      console.log(this.listaTemas);

    }
  }

  
verUnForo(id : number){
  console.log('entra a ver un Foro');
  console.log(id, 'este es el id');

  let encontrado = this._foroSVC.obtenerUnForo(id)
  console.log(encontrado);
  
  
  if(encontrado === undefined){
    this.html = ''
  } else{
    this.html = encontrado.foro;
    this.foro = encontrado
    this.router.navigate(['/dashboard/ver-foro', id])
  }
  console.log(encontrado);
  
  return this.foro
}

crearForo(number: number){
  this.router.navigate(['/dashboard/crear-foro/', number])
}

//CREAR TEMAS
crearTema(){
}

//MOSTRAR USUARIOS REGISTRADOS
traerUsuarios(){
  
this.listaUsuarios = this._usuariosSVC.obtenerLocalStorage();
console.log(this.listaUsuarios);

}

}
