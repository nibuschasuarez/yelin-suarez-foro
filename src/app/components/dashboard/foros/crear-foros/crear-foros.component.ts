import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Editor, Toolbar } from 'ngx-editor';
import { comentariosI, foroI } from 'src/app/interfaces/all-interface.interface';
import { ForosService } from 'src/app/services/foros.service';
import { UsuariosService } from 'src/app/services/usuarios.service';

@Component({
  selector: 'app-crear-foros',
  templateUrl: './crear-foros.component.html',
  styleUrls: ['./crear-foros.component.css']
})
export class CrearForosComponent implements OnInit {

  username!: string;
  IDforo!: number
  
  seccion: any[] = ['Grupos Masculinos', 'Grupos Femeninos'];

  Foro!: foroI;
  foro = {
    titulo: ''
  }

  html = '<p>Escriba aqui</p>';
  editor: Editor;
  toolbar: Toolbar = [
    ['bold', 'italic'],
    ['underline', 'strike'],
    ['code', 'blockquote'],
    ['ordered_list', 'bullet_list'],
    [{ heading: ['h1', 'h2', 'h3', 'h4', 'h5', 'h6'] }],
    ['link', 'image'],
    ['text_color', 'background_color'],
    ['align_left', 'align_center', 'align_right', 'align_justify'],
  ]


  constructor(private _usuariosSvc : UsuariosService,
              private _foroSvc : ForosService,
              private fb: FormBuilder, 
              private router : Router,
              private activateRoute : ActivatedRoute) {
    this.editor = new Editor(); //se instancia el editor

    
    let foroID = Math.floor(Math.random() * 20)
    console.log(foroID);

    let nombre = this._usuariosSvc.mostrarUser();
    console.log(nombre);

    let listaForo : foroI[] = this._usuariosSvc.obtenerLocalStorage()
    console.log(listaForo);
    let coincidencia = listaForo.find(e => e.foroid !== foroID)
    console.log(coincidencia);
    

    //EDITAR UN FORO
    this.activateRoute.params.subscribe(params => {
      const id : number = params['id'];
      let number: number = Number(id);
      this.IDforo = number
      console.log(number);
      console.log('entra a ver un Foro');

      if(number > 0){
        
      let encontrado = this._foroSvc.obtenerUnForoBorrador(number);
      console.log(encontrado);
      
      if(encontrado === undefined){
        this.html = 'FORO NO ENCONTRADO'
      } else{
      this.foro = encontrado;
      this.html = encontrado.foro;
      }
      }
    });

   }

  ngOnInit(): void {
  }

guardar(){
    let fecha = new Date();

    let foroID = Math.floor(Math.random() * 1000)
    console.log(foroID);
    
    let nombre = this._usuariosSvc.mostrarUser();
    let listaForo : foroI[] = this._usuariosSvc.obtenerLocalStorage()
    console.log(listaForo);

    while (listaForo.find(e => e.foroid === foroID)){
      console.log('es igual');
      foroID = Math.floor(Math.random() * 20)
    }

    if(nombre === null){
      console.log('no se pudo');
    } else {
      console.log(foroID);
      
      this.Foro = {
        foroid: foroID,
        usuario: nombre,
        seccion: 0,
        titulo: this.foro.titulo,
        fecha: fecha.toLocaleDateString(),
        foro: this.html
      };
      console.log(this.Foro);

    } 
    this.subirAlLocal(this.Foro);
    this.router.navigate(['/dashboard/foro'])
  }

  traerUsuario(){
   let nombre = this._usuariosSvc.mostrarUser()
  return nombre
  }

  guardarBorrador(){
    let fecha = new Date();

    let foroID = Math.floor(Math.random() * 1000)
    console.log(foroID);
    
    let nombre = this._usuariosSvc.mostrarUser();
    let listaForo : foroI[] = this._usuariosSvc.obtenerLocalStorage()
    console.log(listaForo);

    while (listaForo.find(e => e.foroid === foroID)){
      console.log('es igual');
      foroID = Math.floor(Math.random() * 20)
    }

    if(nombre === null){
      console.log('no se pudo');
    } else {
      console.log(foroID);
      
      this.Foro = {
        foroid: foroID,
        usuario: nombre,
        seccion: 0,
        titulo: this.foro.titulo,
        fecha: fecha.toLocaleDateString(),
        foro: this.html
      };
      console.log(this.Foro);

    } 
    this.subirSession(this.Foro);
    this._usuariosSvc.snackbar(2)

  }

  subirAlLocal(foro: foroI){
    this._foroSvc.agregarNew(foro)
  }
  
  subirSession(foro: foroI){
    this._foroSvc.agregarBorrador(foro)
    // this._foroSvc.hayBorradores()
  }
}
