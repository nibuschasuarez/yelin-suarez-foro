import { Component, OnInit } from '@angular/core';
import { AbstractControlOptions, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UsuariosService } from 'src/app/services/usuarios.service';
import { ValidadoresService } from 'src/app/services/validadores.service';

@Component({
  selector: 'app-registrar',
  templateUrl: './registrar.component.html',
  styleUrls: ['./registrar.component.css']
})
export class RegistrarComponent implements OnInit {

  sexo: any[] = ['Masculino', 'Femenino'];

  form!: FormGroup;

  constructor(private fb: FormBuilder,
              private _validadores: ValidadoresService,
              private _usuarioSVC: UsuariosService,
              private router : Router) {
  this.crearFormulario();
  }

  ngOnInit(): void {
  }

  get nombreNoValido(){
    return this.form.get('nombre')?.invalid && this.form.get('nombre')?.touched
  }
  get correoNoValido(){
    return this.form.get('correo')?.invalid && this.form.get('correo')?.touched
  }

  get pass1NoValido(){
    return this.form.get('pass1')?.invalid && this.form.get('pass1')?.touched;
  }

  get pass2NoValido(){
    const pass1 = this.form.get('pass1')?.value;
    const pass2 = this.form.get('pass2')?.value;
    return (pass1 === pass2) ? false: true;
  }

  get usuarioNoValido(){
    return (this.form.get('usuario')?.invalid && !this.form.get('usuario')?.errors?.['existe']) && this.form.get('usuario')?.touched;
  }

  get usuarioExistente(){
    return this.form.get('usuario')?.errors?.['existe']
  }

  get emailExistente(){
    return this.form.get('correo')?.errors?.['existe']
  }


  crearFormulario():void {
    this.form = this.fb.group({
      nombre: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(30)]],
      correo: ['', [Validators.required,Validators.pattern(/^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/)]],
      usuario: ['', [Validators.required, Validators.minLength(4)]],
      genero: ['', Validators.required],
      pass1: ['', Validators.required],
      pass2: ['', Validators.required]
    }, {
      Validators: this._validadores.passwordsIguales('pass1', 'pass2')
    } as AbstractControlOptions );
  }

  guardar(): void {
    console.log(this.form.value);

    console.log('usuario registrado');
    
    this._usuarioSVC.agregarNew(this.form.value)
    this.LimpiarFormulario()
    this.router.navigate(['/login'])
    //Reset del formulario


  }

  LimpiarFormulario(){
    //this.forma.reset(); //este nos limpia todos los elementos
    this.form.reset({
      nombre: 'listo'
    }); //asi se limpia todo menos este, pero tiene que tener informacion
  }



}
