import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UsuarioI, UsuarioRegistradoI } from 'src/app/interfaces/all-interface.interface';
import { UsuariosService } from 'src/app/services/usuarios.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form!: FormGroup;
  loading: boolean = false;
  lista = [];
  constructor(private fb: FormBuilder,
              private router: Router,
              private _usuariosService: UsuariosService) {
    this.formulario();

    // this.lista = this._usuariosService.obtenerLocalStorage();
    // console.log(this.lista);
    }

  ngOnInit(): void {
  }

  get usuarioNoValido(){
    return (this.form.get('usuario')?.invalid && !this.form.get('usuario')?.errors?.['existe']) && this.form.get('usuario')?.touched;
  }
  
  get pass1NoValido(){
    return this.form.get('pass1')?.invalid && this.form.get('pass1')?.touched;
  }

  formulario(): void {
    this.form = this.fb.group({
      usuario: ['', Validators.required],
      pass1: ['', Validators.required]
    })
  }

  ingresar(): void {
    console.log(this.form.value);

    const Usuario : UsuarioI = {
      usuario: this.form.value.usuario,
      pass1: this.form.value.pass1
    }

    console.log(Usuario);

    this._usuariosService.loginDatos(Usuario)
    
    const lista: UsuarioRegistradoI[] = JSON.parse(localStorage.getItem('Usuarios_Registrados') || '');
    console.log(lista);

    let comparando_contrasenas = lista.find(e => e.pass1 === Usuario.pass1)
    console.log(comparando_contrasenas);
    
    // lista.find(e => e.usuario === Usuario.usuario)

    if( lista.find(e => e.pass1 === Usuario.pass1) && lista.find(e => e.usuario === Usuario.usuario)){
      console.log('Usuario Logeado');
      
      this.fakeLoading();
    } else {
      this._usuariosService.snackbar(1);
      this.form.reset();
    }
    
  }

  fakeLoading(){
    this.loading = true
    setTimeout(() => {
      this.loading = false;

      //Redireccionamos al dashboard
      this.router.navigate(['dashboard']);
    }, 1500);
  }

}
