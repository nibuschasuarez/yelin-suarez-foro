import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UsuarioI, UsuarioRegistradoI } from '../interfaces/all-interface.interface';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

  //LOGIN
  user!: UsuarioI;
  loading: boolean = false;
  nombre_usuario!: string;

  //REGISTRO
  listaRegistro!: UsuarioRegistradoI[];

  constructor(private _snackbar: MatSnackBar) { }

  
  snackbar(number: number):void {
    if(number === 1){
      
    this._snackbar.open('usuario o contraseña incorrecto','',{
      duration: 5000,
      horizontalPosition: 'center',
      verticalPosition: 'top'
    });
    } else if (number === 2){
      
    this._snackbar.open('Borrador guardado!', '',{
      duration: 3000,
      horizontalPosition: 'center',
      verticalPosition: 'top'
    });
    } else if (number === 3){
      
      this._snackbar.open('Comentario publicado!', '',{
        duration: 3000,
        horizontalPosition: 'center',
        verticalPosition: 'top'
      });
    }
  }

  //LOGIN DATOS
  loginDatos(user: UsuarioI){
    console.log(user);
    
    this.user = user;
    console.log(this.user);
    
    sessionStorage.setItem('Usuario_name', this.user.usuario);

    console.log('subido al session storage');

    let nombre = sessionStorage.getItem('Usuario_name')
    console.log(nombre, 'get');
    return  nombre;
  }

  mostrarUser(){
    let nombre = sessionStorage.getItem('Usuario_name')
    return nombre
  }

  cerrarSesion(){
    sessionStorage.removeItem('Usuario_name');
  }

  //ERROR
  obtenerLocalStorage(){

    if(localStorage.getItem("Usuarios_Registrados") === null){
      console.log('no comments');
      this.listaRegistro = [{
        nombre: '',
        correo: '',
        usuario: '',
        genero: '',
        pass1: '',
        pass2: ''
      }] 
      return false
    } else {
      console.log('podria dar error');
      
      let citaLista = JSON.parse(localStorage.getItem("Usuarios_Registrados") || '');
    console.log(citaLista);
    
    this.listaRegistro = citaLista
    // console.log(citaLista, 'obtener LocalStorage');
    return citaLista
    }
  }

  //REGISTRO
  agregarNew(usuario: UsuarioRegistradoI){
    console.log(usuario);
    if(localStorage.getItem('Usuarios_Registrados') === null){
      this.listaRegistro = [];
      this.listaRegistro.unshift(usuario);
      localStorage.setItem('Usuarios_Registrados', JSON.stringify(this.listaRegistro))
    } else {
      this.listaRegistro = JSON.parse(localStorage.getItem('Usuarios_Registrados') || '');
      this.listaRegistro.unshift(usuario);
      localStorage.setItem('Usuarios_Registrados', JSON.stringify(this.listaRegistro))
    } 
    // console.log(this.listaRegistro, 'lista de agregarTablas');
  }
}
