import { TestBed } from '@angular/core/testing';

import { ForoGuardGuard } from './foro-guard.guard';

describe('ForoGuardGuard', () => {
  let guard: ForoGuardGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(ForoGuardGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
